# How to create application manifests

Create a file that has same name as the `name` of the manifest. Make sure you save the file with an extension of `.yml` or `.yaml`. Find an example below.


**app01.yml**

```yml
name: app01
partition: prod
location: dmz
comments: This is a new web server for testing
type: http
virtual_server:
  ip: 10.1.10.152
  port: 80
members:
  - ip: 10.1.20.21
    port: 30880
```
